# Kodi Media Center language file
# Addon Name: VBox TV Gateway PVR Client
# Addon id: pvr.vbox
# Addon Provider: Sam Stenvall
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: https://github.com/xbmc/xbmc/issues/\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Kodi Translation Team\n"
"Language-Team: Chinese (China) (http://www.transifex.com/projects/p/kodi-main/language/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgctxt "#30000"
msgid "Local network"
msgstr "局域网"

msgctxt "#30001"
msgid "Hostname or IP address"
msgstr "主机名或 IP 地址"

msgctxt "#30002"
msgid "HTTP port"
msgstr "HTTP 端口"

msgctxt "#30003"
msgid "UPnP port"
msgstr "UPnP 端口"

msgctxt "#30006"
msgid "Connection"
msgstr "连接"

msgctxt "#30007"
msgid "Internet"
msgstr "互联网"

msgctxt "#30021"
msgid "Channels"
msgstr "频道"

msgctxt "#30022"
msgid "EPG"
msgstr "电子节目单"

msgctxt "#30023"
msgid "Channel numbers set by"
msgstr "频道号设置"

msgctxt "#30024"
msgid "LCN (Logical Channel Number) from backend"
msgstr "LCN（逻辑频道号）来自后端"

msgctxt "#30025"
msgid "Channel index in backend"
msgstr "后端频道索引"

msgctxt "#30026"
msgid "Reminder time (minutes before program starts)"
msgstr "提醒时间（节目开始前多少分钟）"

msgctxt "#30040"
msgid "Timeshift"
msgstr "时光平移"

msgctxt "#30041"
msgid "Enable timeshifting"
msgstr "启用时光平移"

msgctxt "#30042"
msgid "Timeshift buffer path"
msgstr "时光平移缓存路径"

msgctxt "#30106"
msgid "VBox device rescan of EPG (will take a while)"
msgstr "VBox 设备重扫描电子节目单（需要一点时间）"

msgctxt "#30107"
msgid "Sync EPG"
msgstr "同步电子节目单"

msgctxt "#30110"
msgid "Remind me"
msgstr "提醒我"

msgctxt "#30111"
msgid "Manual reminder"
msgstr "手动提醒"

msgctxt "#30112"
msgid "Cancel reminder (if exists)"
msgstr "取消提醒（如存在）"

msgctxt "#30113"
msgid "Cancel all the channel's reminders"
msgstr "取消所有频道提醒"
