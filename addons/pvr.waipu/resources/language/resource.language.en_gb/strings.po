# Kodi Media Center language file
# Addon Name: PVR waipu.tv Client
# Addon id: pvr.waipu
# Addon Provider: flubshi
msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_GB\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgctxt "#30001"
msgid "General"
msgstr ""

msgctxt "#30002"
msgid "Mail"
msgstr ""

msgctxt "#30003"
msgid "Password"
msgstr ""

msgctxt "#30004"
msgid "Protocol"
msgstr ""

msgctxt "#30005"
msgid "Misc"
msgstr ""

msgctxt "#30006"
msgid "InputStream Helper information"
msgstr ""

msgctxt "#30007"
msgid "(Re)install Widevine CDM library..."
msgstr ""

msgctxt "#30008"
msgid "Provider"
msgstr ""

msgctxt "#30009"
msgid "Waipu.tv"
msgstr ""

msgctxt "#30010"
msgid "O2 TV (beta)"
msgstr ""

msgctxt "#30030"
msgid "Error: Login not possible. Check credentials!"
msgstr ""

msgctxt "#30031"
msgid "No network connection"
msgstr ""

msgctxt "#30032"
msgid "Invalid login credentials"
msgstr ""

msgctxt "#30033"
msgid "Required username and password not available"
msgstr ""
